import importlib


class Router:
    def load_module(self, module_name):
        mod = importlib.import_module('{0}.{1}'.format(module_name, 'run'))
        return mod