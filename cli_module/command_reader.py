import re
from share.service_manager import ServiceManager


class Validator:
    def __init__(self):
        self.re_ip = re.compile('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$')
        self.re_version = re.compile('\d{1,2}\.\d{1,3}\.\d{1,3}$')
        self.serviceManager = ServiceManager('')

    def check_ip(self, ip):
        return self.re_ip.match(ip)

    def check_service(self, service_name):
        return service_name in self.serviceManager.pattern

    def check_version(self, version):
        return self.re_version.match(version)


class Commander:
    def __init__(self):
        self.util = Validator()

    def req_param(self, text):
        return str(input(text))

    def get_host(self):
        # Hosts
        Hosts = self.req_param("input target Hosts 127.0.0.1 211.1.1.1 > ").split()
        target_hosts = []
        for host in Hosts:
            if (self.util.check_ip(host)):
                target_hosts.append(host)
            else:
                break

        if not target_hosts:
            raise Exception('host is empty')

        return target_hosts

    def get_sevice(self):
        # Services
        services = self.req_param("input target services default:all > ").split()
        target_services = []

        for service_name in services:
            if (self.util.check_service(service_name)):
                target_services.append(service_name)
            else:
                print('check your service name {0} (exclude)'.format(service_name))

        if not services:
            target_services=ServiceManager.services

        return target_services

    def get_version(self):
        # Version
        version = self.req_param("input services version > ")

        if not(self.util.check_version(version)):
            raise Exception('host is empty')

        return version