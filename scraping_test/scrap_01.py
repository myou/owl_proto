from urllib.request import urlopen
from urllib.request import HTTPError
from bs4 import BeautifulSoup

def getHtml(url):
    try:
        html = urlopen(url)
    except HTTPError as e:
        print(e)
    else:
        print('else')

    return html

def main():
    """

    :return:
    """
    url = "http://www.pythonscraping.com/pages/warandpeace.html"
    html = getHtml(url)
    bsObj = BeautifulSoup(html.read(), "html.parser")

    nameList = bsObj.findAll("span", {"class": "green"})
    for name in nameList:
        print(name.get_text())


if __name__ == "__main__":
    main()