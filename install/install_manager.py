import os
import zipfile, shutil

class Install:
    def __init__(self, svnInfo):
        """

        """
        self.svn_home = svnInfo.svn_home

    def packing(self):
        os.chdir(self.svn_home)
        if os.path.isfile("install.zip"):
            shutil.rmtree("install.zip")

        schedulerZip = zipfile.ZipFile('{0}/{1}'.format(self.svn_home, 'install.zip'), 'w')
        for folder, subfolders, files in os.walk(self.svn_home):
            for file in files:
                schedulerZip.write(os.path.join(folder, file),
                                   os.path.relpath(os.path.join(folder, file),
                                                   self.svn_home),
                                   compress_type=zipfile.ZIP_DEFLATED)

        schedulerZip.close()