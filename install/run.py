import sys
from share import source_builder
import re
from share.service_manager import ServiceManager
from share.transfer_manager import Transfer
from share.config_manager import SvnInfo
from install.install_manager import Install
import os, shutil

class RunUtill:
    def __init__(self):
        self.re_ip = re.compile('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$')
        self.re_version = re.compile('\d{1,2}\.\d{1,3}\.\d{1,3}$')
        self.serviceManager = ServiceManager()

    def check_ip(self, ip):
        return self.re_ip.match(ip)

    def check_service(self, service_name):
        return service_name in self.serviceManager.pattern

    def check_version(self, version):
        return self.re_version.match(version)

class Module:
    def run(self):
        print('123456')

def main():
    args = sys.argv

    if len(args) < 2:
        print('Usage: python3 run.py [target_hosts] [EER_VERSION]')
        exit(-1)

    del args[0]

    #이중화 처리 필요
    util = RunUtill()

    #api화 필요
    # Hosts
    target_hosts = []
    for host in args:
        if(util.check_ip(host)):
            target_hosts.append(host)
        else:
            break

    target_version = '1.0.1'
    for version in args:
        if(util.check_version(version)):
            target_version = version

    srcdir = "D:\\temp\\setup"
    if not os.path.isdir(srcdir):
        os.mkdir(srcdir)

    # svn update - storage info
    svninfo = SvnInfo(svn_home=srcdir, svn_url='https://subversion.spectra.co.kr/svn/DEPT/OSM/04_install_file/국내', svn_id='myou', svn_pw='tmvprxmfk')
    source = source_builder.Svn(svnInfo=svninfo)
    source.update()

    #target_hosts
    deploy = Install(svninfo)
    deploy.packing()

    # file upload
    transfer = Transfer(target_hosts, svninfo)
    transfer.target_file_upload(target_services=target_services)


if __name__ == "__main__":
    main()