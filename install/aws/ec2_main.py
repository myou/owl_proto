from ec2_cli import Luncher, ACL, Instance, ElasticIp, InstanceInfo

def main():
    """

    :return:
    """
    #ec2_luncher = Luncher(['i-03fc4ba10677e7062'])
    #ec2_luncher.start();
    #ec2_luncher.stop();
    #ec2_luncher.reboot();

    ec2_acl = ACL()
    acl_group_id = ec2_acl.create(GroupName='Ansible_test',  Description='Ansible_test')
    permissions = [
        {'IpProtocol': 'tcp',
         'FromPort': 80,
         'ToPort': 80,
         'IpRanges': [{'CidrIp': '0.0.0.0/0'}]},
        {'IpProtocol': 'tcp',
         'FromPort': 22,
         'ToPort': 22,
         'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}
    ]
    ec2_acl.set(GroupId=acl_group_id, permissions=permissions)

    imageId = 'ami-d9b616b7'
    volumeSetting = [
                {
                    'DeviceName': '/dev/xvda',
                    'Ebs': {
                        'VolumeSize': 10,
                        'VolumeType': 'gp2'
                    },
                },
            ]

    securityGroup = acl_group_id
    keyName = 'spectra_test'
    instanceType = 't2.micro'

    IInfo = InstanceInfo(blockDeviceMappings=volumeSetting, securityGroupIds=[securityGroup], imageId=imageId, keyName=keyName, instanceType=instanceType)

    ec2_instance = Instance()
    instanceId = ec2_instance.create(IInfo)
    instance = ec2_instance.get_instances(instanceIds =[instanceId])
    tag = [{'Key': 'Name', 'Value':'Ansible_test'}]
    ec2_instance.attach_tag(instances=instance, tags=tag)

    ec2_elasticIp = ElasticIp()
    ip, allocationId = ec2_elasticIp.create()
    ec2_elasticIp.set(instanceId=instanceId, allocationId=allocationId)
    print(ip)


if __name__ == "__main__":
    main()