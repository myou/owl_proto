import boto3
from botocore.exceptions import ClientError

# pre process
# aws configure
# key
# secret key
# region
# output format (table)

# create user https://linuxacademy.com/howtoguides/posts/show/topic/14209-automating-aws-with-python-and-boto3
# see https://console.aws.amazon.com/iam/home?region=ap-northeast-2#security_credential

class InstanceInfo:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

class Instance:
    def __init__(self):
        self.ec2 = boto3.resource('ec2')

    def create(self, instanceInfo):
        instance = self.ec2.create_instances(
            BlockDeviceMappings=instanceInfo.blockDeviceMappings,
            SecurityGroupIds=instanceInfo.securityGroupIds,
            ImageId=instanceInfo.imageId,
            KeyName=instanceInfo.keyName,
            MinCount=1,
            MaxCount=1,
            InstanceType=instanceInfo.instanceType)
        return instance[0].id

    def attach_tag(self, instances, tags):
        for instance in instances:
            instance.create_tags(Tags=tags)

    def get_instances(self, instanceIds):
        return self.ec2.instances.filter(InstanceIds=instanceIds)



class Luncher:
    def __init__(self, instanceIds):
        self.instanceIds = instanceIds
        self.ec2 = boto3.client('ec2')

    def stop(self):
        # Do a dryrun first to verify permissions
        try:
            self.ec2.stop_instances(InstanceIds=self.instanceIds, DryRun=True)
        except ClientError as e:
            if 'DryRunOperation' not in str(e):
                raise

        # Dry run succeeded, run stop_instances without dryrun
        try:
            response = self.ec2.stop_instances(InstanceIds=self.instanceIds, DryRun=False)
            print(response)
        except ClientError as e:
            print(e)

    def start(self):
        # Do a dryrun first to verify permissions
        try:
            self.ec2.start_instances(InstanceIds=self.instanceIds, DryRun=True)
        except ClientError as e:
            if 'DryRunOperation' not in str(e):
                raise

        # Dry run succeeded, run start_instances without dryrun
        try:
            response = self.ec2.start_instances(InstanceIds=self.instanceIds, DryRun=False)
            print(response)
        except ClientError as e:
            print(e)

    def reboot(self):
        try:
            self.ec2.reboot_instances(InstanceIds=self.instanceIds, DryRun=True)
        except ClientError as e:
            if 'DryRunOperation' not in str(e):
                print("You don't have permission to reboot instances.")
                raise

        try:
            response = self.ec2.reboot_instances(InstanceIds=self.instanceIds, DryRun=False)
            print('Success', response)
        except ClientError as e:
            print('Error', e)

class ACL:
    def __init__(self):
        self.ec2 = boto3.client('ec2')

    def create(self,GroupName, Description):
        response = self.ec2.describe_vpcs()
        vpc_id = response.get('Vpcs', [{}])[0].get('VpcId', '')
        try:
            response = self.ec2.create_security_group(GroupName=GroupName,
                                                 Description=Description,
                                                 VpcId=vpc_id)
            print('Security Group Created %s in vpc %s.' % (response['GroupId'], vpc_id))
            return response['GroupId']

        except ClientError as e:
            print(e)

    def set(self, GroupId, permissions):
        try:
            data = self.ec2.authorize_security_group_ingress(
                GroupId=GroupId,
                IpPermissions=permissions)
            print('Ingress Successfully Set %s' % data)
        except ClientError as e:
            print(e)

class ElasticIp:
    def create(self):
        client = boto3.client('ec2')
        addr = client.allocate_address(Domain='vpc')
        return addr['PublicIp'], addr['AllocationId']

    def set(self,instanceId, allocationId):
        client = boto3.client('ec2')
        addr = client.associate_address(InstanceId=instanceId, AllocationId=allocationId)
