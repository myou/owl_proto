from share.command_excuter import Excuter
import time

class PM:

    def __init__(self, target_host, enomix_home):
        self.enomix_home = enomix_home
        self.excuter = Excuter(target_host)

    def start_pm(self):

        stop_command = ['cd {0}/{1}'.format(self.enomix_home, 'bin'), './all_stop.sh']
        self.excuter.remote_command(stop_command)

        time.sleep(2)

        command = ['cd {0}/{1}'.format(self.enomix_home, 'bin'), './status.sh']
        result = self.excuter.remote_command(command)
        print(result.get_result())
        result.check_string('down')

        time.sleep(3)

        start_command = ['cd {0}/{1}'.format(self.enomix_home, 'bin'), './all_run.sh']
        self.excuter.remote_bg_command(start_command)

        time.sleep(2)

        command = ['cd {0}/{1}'.format(self.enomix_home, 'bin'), './status.sh']
        result = self.excuter.remote_command(command)
        print(result.get_result())
        result.check_string('live')