from PM.PM_manager import PM
from cli_module.command_reader import Commander
from share.config_manager import ServerSvnConfig

class Module:
    def __init__(self):
        config = ServerSvnConfig()
        self.commander = Commander()
        self.enomix_home = config.get(section="SERVER_INFO", key="ENOHOME_HOME")

    def run(self):
        # 이중화 처리 필요
        target_hosts = self.commander.get_host()
        pm = PM(target_hosts, self.enomix_home)
        pm.start_pm()