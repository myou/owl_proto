class SiteHistory:
    def __init__(self):
        """

        """

    def get_create_schema(self):
        return "CREATE TABLE t_site_history (date char(12), type varchar(512), key varchar(512),data varchar(4096))"

    def get_key_schema(self):
        return "ALTER TABLE t_site_history ADD CONSTRAINT PK_SITE_HISTORY PRIMARY KEY (date,type,key)"

    def get_table_schema(self):
        return {"key": 0, "date": 1, "type": 2, "data": 3}

    def get_table_name(self):
        return "t_site_history"
