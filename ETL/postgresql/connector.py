import psycopg2

class Sqltemplate:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
        self.con = psycopg2.connect(host=self.host, port=self.port, database=self.database, user=self.user,
                                    password=self.password)
        self.cur = self.con.cursor()

    def getConnection(self):
        return self.con

    def getCur(self):
        return self.cur

    def select(self, query):
        self.cur.execute(query)
        return self.cur.fetchall()

    def select_one(self, query):
        self.cur.execute(query)
        return self.cur.fetchone()[0]

    def delete(self, query):
        self.cur.execute(query)

    def update(self, query):
        self.cur.execute(query)

    def excuteList(self, query, list):
        self.cur.executemany(query, list)

    def excute(self, query):
        self.cur.execute(query)

    def commit(self):
        self.con.commit()

    def rollback(self):
        self.con.rollback()

    def close(self):
        self.con.close()
        self.cur.close()

    def getTableSchema(self, target_table):
        schemaquery = "SELECT column_name FROM information_schema.columns WHERE table_name = '{0}';".format(
            target_table)
        self.cur.execute(schemaquery)
        resultcolums = self.cur.fetchall()

        return resultcolums

    def getInsertTableSchema(self, target_table):
        resultcolums = self.getTableSchema(target_table)
        colums = []
        values = []
        for colum in resultcolums:
            colums.append(colum[0])
            values.append('%s')

        return colums, values