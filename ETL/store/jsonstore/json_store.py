import json
import os


def hostlist():
    dir = os.path.join(os.path.dirname(__file__), 'hostdata/host.json')

    with open(dir) as json_file:
        json_data = json.load(json_file)

    return json_data.keys(), json_data
