from postgresql.connector import Sqltemplate
from postgresql.table import *
from service.copy_data import Copydata
from service.decrypt_data import Decryptdata
from rdb_query.query_builder import Querybuilder
from service.remove_personal_information import RPI
from store.load_host import gethostlist
from datetime import datetime, timedelta
import jpype #@UnresolvedImport
import sys
from service.history_data import Historydata

def main():
    args = sys.argv
    del args[0]
    run_type = "daily"

    if len(args) > 0:
        run_type = args[0]
        del args[0]

    site_list, con_info = gethostlist()

    #daily
    today = datetime.today() - timedelta(1)
    today = today.strftime("%Y%m%d")
    startdate = today + "000000"
    enddate = today + "235959"

    #realtime
    timestamp = datetime.today().strftime("%Y%m%d%H%M")

    print("copy data range ::: {0} - {1}".format(startdate, enddate))

    for site in site_list:
        site_info = con_info[site]

        sourceSqltemplate = Sqltemplate(host=site_info["host"],
                                        port=site_info["port"],
                                        database=site_info["database"],
                                        user=site_info["user"],
                                        password=site_info["password"])

        targetSqltemplate = Sqltemplate(host='52.78.192.186',
                                        port='5432',
                                        database=site_info["targetname"],
                                        user='PA',
                                        password='tmvprxmfk')

        print("copy data site info ::: {0} to {1}".format(site, site_info["targetname"]))

        if run_type == "daily":
            daily_process(sourceSqltemplate=sourceSqltemplate, targetSqltemplate=targetSqltemplate,
                          initdata={"startdate": startdate, "enddate": enddate, "encrypt": site_info["encrypt"]})
            jpype.shutdownJVM()

        if run_type == "realtime":
            realtime_process(sourceSqltemplate=sourceSqltemplate, targetSqltemplate=targetSqltemplate,
                             initdata={"timestamp": timestamp})

        # 연결을 종료한다
        sourceSqltemplate.close()
        targetSqltemplate.close()

def realtime_process(sourceSqltemplate, targetSqltemplate, initdata):
    # 대상에 테이블이 없는 경우 생성
    querybuilder = Querybuilder()
    table_exsist = targetSqltemplate.select_one(querybuilder.get_table_exist_query("t_site_history"))
    if not table_exsist:
        history_table = SiteHistory()
        targetSqltemplate.excute(history_table.get_create_schema())
        targetSqltemplate.excute(history_table.get_key_schema())
        targetSqltemplate.commit()

    history = Historydata(sourceSqltemplate=sourceSqltemplate, targetSqltemplate=targetSqltemplate,
                          timestamp=initdata["timestamp"])
    history.process()

def daily_process(sourceSqltemplate, targetSqltemplate, initdata):
    createdateCondition = Querybuilder(sqltemplate=sourceSqltemplate)
    createdateCondition.append(target="created_date", condition=">=", value=initdata["startdate"])
    createdateCondition.append(target="created_date", condition="<=", value=initdata["enddate"])

    ticketIds = createdateCondition.getkeyids(key='ticket_id', target_table='t_ticket')

    ticketidCondition = Querybuilder(sqltemplate=sourceSqltemplate)
    ticketidCondition.appendsubquery(target="ticket_id", condition="in", value=ticketIds)

    talkqueryCondition = Querybuilder(sqltemplate=sourceSqltemplate)
    talkqueryCondition.appendsubquery(target="talk_id", condition="in", value=ticketIds)

    allCondition = Querybuilder(sqltemplate=targetSqltemplate)

    tablelist = ['t_ticket', 't_ticket_process_time', 't_ticket_option', 't_talk', 't_talk_message', 't_talk_process',
                 't_report_action_log', 't_account', 't_node', 't_env', 't_env_value', 't_env_item']

    builderdic = {}
    builderdic['t_ticket'] = createdateCondition
    builderdic['t_ticket_process_time'] = ticketidCondition
    builderdic['t_ticket_option'] = ticketidCondition
    builderdic['t_talk'] = talkqueryCondition
    builderdic['t_talk_message'] = talkqueryCondition
    builderdic['t_talk_process'] = talkqueryCondition
    builderdic['t_report_action_log'] = createdateCondition
    builderdic['t_account'] = allCondition
    builderdic['t_node'] = allCondition
    builderdic['t_env'] = allCondition
    builderdic['t_env_value'] = allCondition
    builderdic['t_env_item'] = allCondition

    copydata = Copydata(source=sourceSqltemplate, target=targetSqltemplate)

    # unsed table clear
    accountlist = ['t_login_history', 't_account_property_value ', 't_macro']
    nodelist = ["t_domain_channel_rel", "t_domain_codeset_rel", "t_node_survey_rel", "t_survey_item", "t_survey",
                "t_template_contents", "t_template", "t_node_service", "t_notice_reader", "t_notice",
                "t_work_time_weekly", "t_work_time", "t_node_kb_rel", "t_node_account_rel", "t_kb_search_log", "t_kb_view_log"]
    copydata.all_data_clear(accountlist)
    copydata.all_data_clear(nodelist)

    # copydata run
    copydata.run(tablelist, builderdic)

    # 고객정보 삭제
    rpi = RPI(target=targetSqltemplate)
    rpi.run(['t_ticket'], builderdic)

    # 메시지 복호화
    if initdata["encrypt"] == 'Y':
        decryptdata = Decryptdata(target=targetSqltemplate)
        decryptdata.run(['t_talk_message'], builderdic)

if __name__ == "__main__":
    main()