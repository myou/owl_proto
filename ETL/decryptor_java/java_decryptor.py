import jpype #@UnresolvedImport
import os.path


class Javadecryptor():
    def __init__(self):
        if not jpype.isJVMStarted():
            self.dec = ''

    def jvmload(self):
        if not jpype.isJVMStarted():
            jarpath = os.path.join(os.path.dirname(__file__), 'jar')
            path = str.join(";", [jarpath + "/" + name for name in os.listdir(jarpath)])
            #path = str.join(":", [jarpath + "/" + name for name in os.listdir(jarpath)]) #for linux lib

            jpype.startJVM(jpype.getDefaultJVMPath(), "-ea", "-Xmx512M", "-Djava.class.path=%s" % path)

        package = jpype.JPackage('decrypt')  # get the package
        decryptor = package.EERDecryptP  # get the class
        self.dec = decryptor()  # create an instance of the class

    def jvmdown(self):
        jpype.shutdownJVM()

    def decrypt(self, text):
        try:
            return self.dec.decrypt(text)
        except Exception as e:
            return text
