class RPI:
    def __init__(self, target):
        self.target_sqltemplate = target

    def remove(self, target_table, query_condition):
        query = query_condition.getupdatequery(target_table=target_table, target_column=self.gettargetcolumn())
        self.target_sqltemplate.update(query)
        self.target_sqltemplate.commit()

    def gettargetcolumn(self):
        customer_column_list = ['customer_ip', 'customer_email', 'customer_name', 'customer_tel', 'customer_no']
        for index, value in enumerate(customer_column_list):
            customer_column_list[index] = value + "=null"
        target_column = " , ".join(customer_column_list)

        return target_column


    def run(self, table_list, builder_dict):
        for table in table_list:
            self.remove(target_table=table, query_condition=builder_dict[table])

