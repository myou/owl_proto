from rdb_query.query_builder import Querybuilder

class Account:
    def __init__(self, sourceSqltemplate):
        self.query = Querybuilder(sqltemplate=sourceSqltemplate)
        self.source_sqltemplate = sourceSqltemplate

    def gat_data(self, date):
        target_table = "t_account"
        self.query.append(target="delete_flag", condition="=", value="N")
        self.query.append(target="role_id", condition="in", value=["ACCTAGT", "ACCTMGR"])
        datarows = self.source_sqltemplate.select(self.query.getselectquery(target_table=target_table))

        resultcolums = self.source_sqltemplate.getTableSchema(target_table=target_table)
        ididx = [x for x, y in enumerate(resultcolums) if y[0] == 'account_id'][0]
        loginidx = [x for x, y in enumerate(resultcolums) if y[0] == 'login_flag'][0]
        routingidx = [x for x, y in enumerate(resultcolums) if y[0] == 'cstalk_routing_flag'][0]

        try:
            answernumidx = [x for x, y in enumerate(resultcolums) if y[0] == 'cstalk_concurrent_answer_num'][0]
        except:
            answernumidx = [x for x, y in enumerate(resultcolums) if y[0] == 'concurrent_answer_num'][0]

        result = []
        key_list = []
        for data in datarows:
            json_data = {"key": data[ididx], "type": "account", "date": date,
                         "data": {"login_flag": data[loginidx], "cstalk_routing_flag": data[routingidx], "cstalk_concurrent_answer_num": str(data[answernumidx])}}
            key_list.append(data[ididx])
            result.append(json_data)

        return key_list, result
