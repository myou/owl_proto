from rdb_query.query_builder import Querybuilder
import json

class Node:
    def __init__(self, sourceSqltemplate):
        self.query = Querybuilder(sqltemplate=sourceSqltemplate)
        self.source_sqltemplate = sourceSqltemplate

    def gat_data(self, date):
        target_table = "t_domain_account_rel"
        self.query.append(target="service_type", condition="=", value="SVTLK")
        datarows = self.source_sqltemplate.select(self.query.getselectquery(target_table=target_table))

        resultcolums = self.source_sqltemplate.getTableSchema(target_table=target_table)
        keyidx = [x for x, y in enumerate(resultcolums) if y[0] == 'domain_id'][0]
        accountidx = [x for x, y in enumerate(resultcolums) if y[0] == 'account_id'][0]

        values = set(map(lambda x: x[keyidx], datarows))
        domain_datarows = [{"key": x, "data": {"account_list": [y[accountidx] for y in datarows if y[keyidx] == x]}} for x in values]

        target_table = "t_node_account_rel"
        datarows = self.source_sqltemplate.select(self.query.getselectquery(target_table=target_table))

        resultcolums = self.source_sqltemplate.getTableSchema(target_table=target_table)
        keyidx = [x for x, y in enumerate(resultcolums) if y[0] == 'node_id'][0]
        accountidx = [x for x, y in enumerate(resultcolums) if y[0] == 'account_id'][0]

        values = set(map(lambda x: x[keyidx], datarows))
        node_datarows = [{"key": x, "data": {"account_list": [y[accountidx] for y in datarows if y[keyidx] == x]}} for x in values]

        temp_datarows = domain_datarows + node_datarows
        result = []
        key_list = []

        for data in temp_datarows:
            json_value = json.dumps(dict(data["data"]))
            json_data = {"key": data["key"], "type": "node", "date": date,
                         "data": json.loads(json_value)}

            key_list.append(data["key"])
            result.append(json_data)

        return key_list, result
