from rdb_query.query_builder import Querybuilder
from postgresql.table import *
import json
import copy


class CopyHistoryData:
    def __init__(self, sourceSqltemplate, targetSqltemplate):
        self.site_history = SiteHistory()
        self.source_sqltemplate = sourceSqltemplate
        self.target_sqltemplate = targetSqltemplate

    def diff(self, type, target, rows):
        query = Querybuilder(self.target_sqltemplate)
        query.append(target="type", condition="=", value=type)
        query.append(target="key", condition="in", value=target)
        datarows = self.target_sqltemplate.select(query.gethistoryquery(target_table=self.site_history.get_table_name(),
                                                                        target="date", type="DESC"))
        idxs = self.site_history.get_table_schema()

        result = []

        for row in rows:
            change = True
            indexes = [i for i, x in enumerate(datarows) if x[idxs["key"]] == row["key"]]

            if indexes:
                source_data = row['data']
                target_data = json.loads(datarows[indexes[0]][idxs["data"]])

                for key in source_data.keys():
                    if str(target_data[key]) != str(source_data[key]):
                        change = True
                        break
                    else:
                        change = False

            if change:
                result.append((row["date"], row["type"], row["key"], json.dumps(row["data"])))

        return result


    def insert(self, rows):
        query = Querybuilder(self.target_sqltemplate)
        if rows:
            self.target_sqltemplate.excuteList(query.getinsertquery(self.site_history.get_table_name()), rows)
            self.target_sqltemplate.commit()
        print("::: {0} insert complete   count {1}:::".format(self.site_history.get_table_name(), len(rows)))