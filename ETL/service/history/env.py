from rdb_query.query_builder import Querybuilder
import json

class Env:
    def __init__(self, sourceSqltemplate):
        self.query = Querybuilder(sqltemplate=sourceSqltemplate)
        self.source_sqltemplate = sourceSqltemplate

    def gat_data(self, date):
        target_table = "t_env_value"
        self.query.append(target="env_id", condition="in",
                          value=["TLK_CS_RECEIPT_OUTOFTIME_FLAG", "TLK_CS_MAX_WAIT_GNR_PROP_FLAG", "TLK_CS_MAX_WAIT_CNT", "TLK_CS_MAX_WAIT_PROPORTION_CNT", "TLK_MAX_WAIT_GNR_PROPORTION_FLAG"])
        datarows = self.source_sqltemplate.select(self.query.getselectquery(target_table=target_table))

        resultcolums = self.source_sqltemplate.getTableSchema(target_table=target_table)
        keyidx = [x for x, y in enumerate(resultcolums) if y[0] == 'target_id'][0]
        envidx = [x for x, y in enumerate(resultcolums) if y[0] == 'env_id'][0]
        valueidx = [x for x, y in enumerate(resultcolums) if y[0] == 'env_value'][0]

        result = []
        key_list = []

        values = set(map(lambda x: x[keyidx], datarows))
        temp_datarows = [{"key": x, "data": [(y[envidx], y[valueidx]) for y in datarows if y[keyidx] == x]} for x in values]

        for data in temp_datarows:
            json_value = json.dumps(dict(data["data"]))
            json_data = {"key": data["key"], "type": "env", "date": date,
                         "data": json.loads(json_value)}

            key_list.append(data["key"])
            result.append(json_data)

        return key_list, result
