from service.history.account import Account
from service.history.env import Env
from service.history.node import Node
from service.history.copy_history_data import CopyHistoryData

class Historydata:
    def __init__(self, sourceSqltemplate, targetSqltemplate, timestamp):
        self.timestamp = timestamp
        self.source_sqltemplate = sourceSqltemplate
        self.target_sqltemplate = targetSqltemplate
        self.copy_history_data = CopyHistoryData(sourceSqltemplate=sourceSqltemplate, targetSqltemplate=targetSqltemplate)

    def process(self):
        account = Account(sourceSqltemplate=self.source_sqltemplate)
        key, rows = account.gat_data(date=self.timestamp)
        rows = self.copy_history_data.diff(type="account", target=key, rows=rows)
        self.copy_history_data.insert(rows=rows)

        env = Env(sourceSqltemplate=self.source_sqltemplate)
        key, rows = env.gat_data(date=self.timestamp)
        rows = self.copy_history_data.diff(type="env", target=key, rows=rows)
        self.copy_history_data.insert(rows=rows)

        node = Node(sourceSqltemplate=self.source_sqltemplate)
        key, rows = node.gat_data(date=self.timestamp)
        rows = self.copy_history_data.diff(type="node", target=key, rows=rows)
        self.copy_history_data.insert(rows=rows)

