from rdb_query.query_builder import Querybuilder

class Copydata:
    def __init__(self, source, target):
        self.source_sqltemplate = source
        self.target_sqltemplate = target


    def datacopy(self, target_table, query_condition):
        datarows = []
        try:
            datarows = self.source_sqltemplate.select(query_condition.getselectquery(target_table=target_table))
            print("::: {0} select complete  size : {1} :::".format(target_table, len(datarows)))

            if datarows:
                self.target_sqltemplate.excuteList(query_condition.getinsertquery(target_table), datarows)
                self.target_sqltemplate.commit()
            print("::: {0} insert complete   :::".format(target_table))
        except Exception as e:
            self.target_sqltemplate.rollback()
            print(e)
            pass

        return datarows

    def tableclear(self, target_table, query_condition):
        try:
            self.target_sqltemplate.delete(query_condition.getdeletequery(target_table))
            self.target_sqltemplate.commit()
        except Exception as e:
            self.target_sqltemplate.rollback()
            print(e)
            pass
        print("::: {0} delete complete   :::".format(target_table))

    def all_data_clear(self, table_list):
        qb = Querybuilder()
        for table in table_list:
            try:
                self.target_sqltemplate.delete(qb.getdeletequery(target_table=table))
                self.target_sqltemplate.commit()
            except Exception as e:
                self.target_sqltemplate.rollback()
                print(e)
                pass

    def run(self, table_list, builder_dict):
        # insert 와 반대로 수행한다. : reserse
        for table in list(reversed(table_list)):
            self.tableclear(target_table=table, query_condition=builder_dict[table])

        for table in table_list:
            self.datacopy(target_table=table, query_condition=builder_dict[table])
