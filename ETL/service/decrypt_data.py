from decryptor_java.java_decryptor import Javadecryptor
from rdb_query.query_builder import Querybuilder

class Decryptdata:
    def __init__(self, target):
        self.target_sqltemplate = target

    def getdate(self, target_table, query_condition):
        datarows = self.target_sqltemplate.select(query_condition.getselectquery(target_table=target_table))
        print("::: {0} select complete  size : {1} :::".format(target_table, len(datarows)))
        return datarows

    def decrypt(self, target_table, datarows):
        qu = Querybuilder(self.target_sqltemplate)

        resultcolums = self.target_sqltemplate.getTableSchema(target_table=target_table)
        messageindex = [x for x, y in enumerate(resultcolums) if y[0] == 'message'][0]
        idindex = [x for x, y in enumerate(resultcolums) if y[0] == 'talk_id'][0]
        seqindex = [x for x, y in enumerate(resultcolums) if y[0] == 'message_seq'][0]

        jc = Javadecryptor()
        jc.jvmload()

        datalist = []
        for data in datarows:
            message = jc.decrypt(data[messageindex])
            message = message.replace("'", "\"")
            datalist.append("('{0}', {1}, '{2}')".format(data[idindex], data[seqindex], message))

        #jc.jvmdown()
        if datalist:
            qstr = qu.getupdatemanyquery(target_table=target_table, datarows=datalist, updatecolumn=["message"], targetcolumn=["talk_id", "message_seq", "message"], keylist=["talk_id", "message_seq"])
            try:
                self.target_sqltemplate.excute(query=qstr)
                self.target_sqltemplate.commit()
            except Exception as e:
                self.target_sqltemplate.rollback()
                print(e)
                pass



    def run(self, table_list, builder_dict):
        for table in table_list:
            datarows = self.getdate(target_table=table, query_condition=builder_dict[table])
            self.decrypt(target_table=table, datarows=datarows)


