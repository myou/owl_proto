class Querybuilder:
    def __init__(self, sqltemplate=None):
        self.sqltemplate = sqltemplate
        self.wherecondition = []

    def getselectquery(self, target_table):
        if self.wherecondition:
            weherestr = " AND ".join(self.wherecondition)
            return "SELECT * FROM {0} WHERE {1}".format(target_table, weherestr)
        else:
            return "SELECT * FROM {0}".format(target_table)

    def getselectticket(self, target_table):
        if self.wherecondition:
            weherestr = " AND ".join(self.wherecondition)
            return "SELECT ticket_id, customer_no FROM {0} WHERE {1}".format(target_table, weherestr)
        else:
            return "SELECT ticket_id, customer_no FROM {0}".format(target_table)

    def getkeyids(self, key, target_table):
        weherestr = " AND ".join(self.wherecondition)
        return "SELECT {0} FROM {1} WHERE {2}".format(key, target_table, weherestr)

    def getdeletequery(self, target_table):
        weherestr = " AND ".join(self.wherecondition)
        if self.wherecondition:
            return "DELETE  FROM {0} WHERE {1}".format(target_table, weherestr)
        else:
            return "DELETE  FROM {0}".format(target_table)

    def getinsertquery(self, target_table):
        colums, values = self.sqltemplate.getInsertTableSchema(target_table)
        return "INSERT INTO {0}({1}) VALUES ({2})".format(target_table, ", ".join(colums), ", ".join(values))

    def getupdatequery(self, target_table, target_column):
        weherestr = " AND ".join(self.wherecondition)
        return "UPDATE {0} SET {1} WHERE {2}".format(target_table, target_column, weherestr)

    def gethistoryquery(self, target_table, target, type="ASC"):
        if self.wherecondition:
            weherestr = " AND ".join(self.wherecondition)
            return "SELECT distinct key, date, type, data FROM {0} WHERE {1} ORDER BY {2} {3}".format(target_table, weherestr, target, type)
        else:
            return "SELECT distinct key, date, type, data FROM {0} ORDER BY {1} {2}".format(target_table, target, type)

    def getupdatemanyquery(self, target_table, updatecolumn, targetcolumn, datarows, keylist):
        update_column = []
        target_key = []
        for column in updatecolumn:
            update_column.append(column + '= c.' + column)

        for key in keylist:
            target_key.append('t.' + key + '= c.' + key)

        update_column = " ".join(update_column)
        target_key = " AND ".join(target_key)
        target_data = " , ".join(datarows)

        return "UPDATE {0} AS t SET {1} FROM (values {2}) as c({3}) WHERE {4}".\
            format(target_table, update_column, target_data, " , ".join(targetcolumn), target_key)

    def get_table_exist_query(self, target_table):
        return "select exists(select * from information_schema.tables where table_name='{0}')".format(target_table)

    def append(self, target, condition, value):
        if isinstance(value, list):
            value = "('" + "', '".join(value) + "')"
            self.wherecondition.append("{0} {1} {2}".format(target, condition, value))
        elif isinstance(value, str):
            self.wherecondition.append("{0} {1} '{2}'".format(target, condition, value))

    def appendsubquery(self, target, condition, value):
        self.wherecondition.append("{0} {1} ({2})".format(target, condition, value))


    def clean(self):
        self.wherecondition = []
