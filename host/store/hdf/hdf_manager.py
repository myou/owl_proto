import h5py

class Store:
    def save(self):
        print('')

    def get(self):
        print('')

    def set(self):
        print('')


def main():
    d1 = ('TEST','date','count')
    hf = h5py.File('data.h5', 'w')
    hf.create_dataset('dataset_1', data=d1)

    hf.close()

    hf = h5py.File('data.h5', 'r')
    hf.keys()

    n1 = hf.get('dataset_1')
    print(n1)


if __name__ == "__main__":
    main()