from share import source_builder
from share.transfer_manager import Transfer
from share.config_manager import SvnInfo
from deploy.deploy_manager import Deploy
from cli_module.command_reader import Commander

class Module:
    def __init__(self):
        self.commander = Commander()

    def run(self):
        # 이중화 처리 필요
        target_hosts = self.commander.get_host()
        target_services = self.commander.get_sevice()

        # svn update - storage info
        svninfo = self.get_svninfo()
        source = source_builder.Svn(svninfo)
        source.update()

        # maven build (clean install)
        builder = source_builder.MavenBuilder(svninfo)
        builder.execute(target_services=target_services)

        # file upload
        transfer = Transfer(target_hosts, svninfo)
        transfer.target_file_upload(target_services=target_services)

        # deploy : stop service - src deploy - start service
        deploy = Deploy(target_hosts, svninfo)
        #deploy.stop_service('engine')
        #deploy.stop_service('webroot')
        #deploy.stop_all_service()
        deploy.execute(target_services=target_services)
        #deploy.start_all_service()
        #deploy.start_service('engine')
        #deploy.start_service('webroot')

        # dual set
        # gateway run
        # deploy.start_service('gateway')
        # other service run (all run)


    def get_svninfo(self):
        # SVN infomation
        svn_home = self.commander.req_param("input svn home(local dir) > ")
        svn_url = self.commander.req_param("input svn_url > ")
        svn_id = self.commander.req_param("input svn id > ")
        svn_pw = self.commander.req_param("input svn password > ")
        version = self.commander.get_version()
        return SvnInfo(svn_home=svn_home, svn_url=svn_url, svn_id=svn_id, svn_pw=svn_pw, version=version)

def main():
    """

    :return:
    """

if __name__ == "__main__":
    main()