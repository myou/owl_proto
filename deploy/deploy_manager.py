from share.command_excuter import Excuter
from share.service_manager import ServiceManager
from share.config_manager import ServerSvnConfig

class Deploy:

    def __init__(self, target_host, svninfo):
        """

        """
        config = ServerSvnConfig()
        self.target_host = target_host
        self.enomix_home = config.get(section="SERVER_INFO", key="ENOHOME_HOME")
        self.app_home = config.get(section="SERVER_INFO", key="APP_HOME")
        self.release_home = config.get(section="SERVER_INFO", key="RELEASE_HOME")
        self.excuter = Excuter(target_host)
        self.service = ServiceManager(svninfo.version)


    def stop_all_service(self):
        """

        :return:
        """
        # all stop service
        stop_command = ['cd {0}/{1}'.format(self.enomix_home, 'bin'), './all_stop.sh']
        self.excuter.remote_command(stop_command)

    def start_all_service(self):
        """

        :return:
        """
        # all start service
        start_command = ['cd {0}/{1}'.format(self.enomix_home, 'bin'), './all_run.sh']
        self.excuter.remote_command(start_command)

    def start_service(self, service_name):
        """

        :return:
        """
        # start service
        start_command = ['cd {0}/{1}'.format(self.enomix_home, 'bin'), './{0}_run.sh'.format(service_name)]
        self.excuter.remote_command(start_command)

    def stop_service(self, service_name):
        """

        :return:
        """
        # start service
        start_command = ['cd {0}/{1}'.format(self.enomix_home, 'bin'), './{0}_stop.sh'.format(service_name)]
        self.excuter.remote_command(start_command)

    def restart_target_service(self, target_services):
        for service in target_services:
            self.stop_service(service)

        for service in target_services:
            self.start_service(service)

    def execute(self, target_services):
        """

        :return:
        """
        self.pre_process();

        for service_name in target_services:
            self.source_deploy(service_name)

        self.post_process();

    def source_deploy(self, service_name):

        war_filename = self.service.translation(service_name, 'target')

        # service delete
        delete_command = ['rm -rf {0}/{1}'.format(self.app_home, service_name)]
        self.excuter.remote_command(delete_command)

        # directory create
        mkdir_command = ['mkdir -p {0}/{1}'.format(self.app_home, service_name)]
        self.excuter.remote_command(mkdir_command)

        # unzip
        zip_file_path = '{0}/{1}/{2}'.format(self.release_home, service_name, war_filename)
        target_path = '{0}/{1}'.format(self.app_home, service_name)

        # 스케줄러 이슈 있음.
        unzip_command = ['unzip {0} -d {1}'.format(zip_file_path, target_path)]

        self.excuter.remote_command(unzip_command)

    def pre_process(self):
        # preprocess
        # bakcup - service_name (gateway, webroot)
        backup_commands = [
            'rm -rf /EER_cloud/_backup',
            'mkdir -p /EER_cloud/_backup',
            'cp -f /EER_cloud/enomix/webapps/gateway/WEB-INF/web.xml /EER_cloud/_backup/',
            #'cp -f /EER_cloud/enomix/webapps/gateway/WEB-INF/config/enomix/thirdparty.properties /EER_cloud/_backup/',
            'cp -R /EER_cloud/enomix/webapps/webroot/WEB-INF/config/enomix /EER_cloud/_backup/'
        ]
        self.excuter.remote_command(backup_commands)

    def post_process(self):
        # postprocess
        # recovery
        recovery_commands = [
            'mv -f /EER_cloud/_backup/web.xml /EER_cloud/enomix/webapps/gateway/WEB-INF',
            #'mv -f /EER_cloud/_backup/thirdparty.properties /EER_cloud/enomix/webapps/gateway/WEB-INF/config/enomix/',
            'mv -f /EER_cloud/_backup/enomix/* /EER_cloud/enomix/webapps/webroot/WEB-INF/config/enomix/'
        ]
        self.excuter.remote_command(recovery_commands)