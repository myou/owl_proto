from fabric.api import run,env,execute,task,roles,sudo,settings
from fabric.operations import local,put
import os
from share.config_manager import ServerSvnConfig

class Excuter:
    def param_deco(func):
        def func_wrapper(param):
            if isinstance(param, str):
                return func(param)
            if isinstance(param, list):
                return func(" && ".join(param))
            raise Exception('command is fault')
        return func_wrapper

    def __init__(self, target_host):
        config = ServerSvnConfig()
        self.target_host = target_host
        env.user = config.get(section="SERVER_INFO", key="SSH_USER_ID")
        env.password = config.get(section="SERVER_INFO", key="SSH_USER_PW")
        env.hosts = target_host
        env.key_filename = config.getKeyfile()

    @task
    @param_deco
    def remote_command(cmd):
        result_builder = ResultBuilder()
        for host in env.hosts:
            with settings(host_string=host):
                output = run(cmd)
                result_builder.build(host=host, output=output)

        return result_builder

    @task
    @param_deco
    def remote_bg_command(cmd):
        result_builder = ResultBuilder()
        for host in env.hosts:
            with settings(host_string=host):
                output = run(cmd, pty=False)
                result_builder.build(host=host, output=output)

        return result_builder

    @task
    def file_upload(source, target):
        for host in env.hosts:
            with settings(host_string=host):
                put(source, target)

    @task
    @param_deco
    def local_command(cmd):
        local(cmd)


class ResultBuilder:
    def __init__(self):
        self.result = {}
        self.check_result = {}

    def build(self, host, output):
        self.result[host] = output.splitlines()

    def get_result(self):
        return self.result

    def check_string(self, check_str):
        for host in self.result:
            if any(check_str in s for s in self.result[host]):
                self.check_result[host] = True
            else:
                self.check_result[host] = False

        return self.check_result
