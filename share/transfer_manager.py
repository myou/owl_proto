from share.config_manager import ServerSvnConfig
from share.service_manager import ServiceManager
from share.command_excuter import Excuter

class Transfer:

    def __init__(self, target_hosts, svnInfo):
        """

        """
        config = ServerSvnConfig()

        self.target_hosts = target_hosts
        self.svn_home = svnInfo.svn_home
        self.release_home = config.get(section="SERVER_INFO", key="RELEASE_HOME")
        self.excuter = Excuter(target_hosts)
        self.service = ServiceManager(svnInfo.version)


    def target_file_upload(self, target_services):
        #  release delete
        delete_command = ['rm -rf {0}'.format(self.release_home)]
        self.excuter.remote_command(delete_command)

        # release directory create
        mkdir_command = ['mkdir -p {0}'.format(self.release_home)]
        self.excuter.remote_command(mkdir_command)

        for service_name in target_services:
            # setup
            source_path = self.service.translation(service_name, 'source')
            target_filename = self.service.translation(service_name, 'target')
            full_war_path = '{0}/{1}/target/{2}'.format(self.svn_home, source_path, target_filename)
            target_path = '{0}/{1}/'.format(self.release_home, service_name)

            # service directory create
            mkdir_command = ['mkdir -p {0}'.format(target_path)]
            self.excuter.remote_command(mkdir_command)

            # file upload start
            self.excuter.file_upload(source=full_war_path, target=target_path)

    def dir_file_upload(self, target_dir):
        #  release delete
        delete_command = ['rm -rf {0}{1}'.format(self.release_home, 'install')]
        self.excuter.remote_command(delete_command)

        # release directory create
        mkdir_command = ['mkdir -p {0}'.format(self.release_home)]
        self.excuter.remote_command(mkdir_command)

        full_war_path = '{0}/{1}/target/{2}'.format(self.svn_home, source_path, target_filename)
        target_path = '{0}/{1}/'.format(self.release_home, service_name)

        # target directory create
        mkdir_command = ['mkdir -p {0}'.format(target_path)]
        self.excuter.remote_command(mkdir_command)

        # file upload start
        self.excuter.file_upload(source=full_war_path, target=target_path)