import os
from share.command_excuter import Excuter
from share.service_manager import ServiceManager

class Svn:

    def __init__(self, svnInfo):
        self.svn_id = svnInfo.svn_id
        self.svn_url = svnInfo.svn_url
        self.svn_pw = svnInfo.svn_pw
        self.svn_home = svnInfo.svn_home
        self.excuter = Excuter('')

    def update(self):
        """

        svn clean checkout|update
        :return:
        """

        if not os.path.isdir(self.svn_home):
            os.mkdir(self.svn_home)

        os.chdir(self.svn_home)

        try:
            self.excuter.local_command('svn cleanup')
        except:
            self.excuter.local_command("svn checkout {0} {1}".format(self.svn_url, self.svn_home))

        self.excuter.local_command('svn update')

        print('=' * 100)
        print('svn update complete!!')
        print('=' * 100)

        return True

class MavenBuilder:

    def __init__(self, svnInfo):
        self.svn_home = svnInfo.svn_home
        self.excuter = Excuter('')
        self.service = ServiceManager(svnInfo.version)

    def execute(self, target_services):
        """

        :return:
        """
        for service_name in target_services:
            self.clean_install(service_name)

        return True

    def clean_install(self, service_name):
        path = os.path.join(self.svn_home, self.service.translation(service_name, 'source'))
        os.chdir(path)

        self.excuter.local_command('mvn clean install')
        if service_name == 'scheduler':
            import zipfile, shutil
            zippath = os.path.join(path, 'target')
            os.chdir(zippath)

            if os.path.isdir("./{0}/".format(service_name)):
                shutil.rmtree('service_name')

            os.mkdir("./{0}/".format(service_name))
            os.mkdir("./{0}/bin".format(service_name))

            shutil.copytree('lib', '{0}/lib'.format(service_name))
            shutil.copy('ecc-10-scheduler-4.0.1.jar', '{0}/bin'.format(service_name))

            schedulerzip = zipfile.ZipFile('{0}/{1}'.format(zippath, self.service.translation(service_name, 'target')), 'w')
            for folder, subfolders, files in os.walk('{0}/{1}/'.format(zippath, service_name)):
                for file in files:
                    schedulerzip.write(os.path.join(folder, file),
                                      os.path.relpath(os.path.join(folder, file), '{0}/{1}/'.format(zippath, service_name)),
                                      compress_type=zipfile.ZIP_DEFLATED)

            schedulerzip.close()

        print('#' * 100)
        print('mvn clean install {} complete!!'.format(service_name))
        print('#' * 100)