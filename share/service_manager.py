class ServiceManager:
    services = ['engine', 'gateway', 'legw', 'restapi', 'router', 'rtis', 'scheduler', 'webapps', 'thirdparty', 'webroot']
    def __init__(self, version):
        self.pattern = {
            'engine': {
                'source': 'supertalk-05-engine',
                'target': 'ecc-05-engine-{0}.war'.format(version),
                'release': 'engine',
                'run': 'engine_run.sh',
                'stop': 'engine_stop.sh',
            },
            'gateway': {
                'source': 'supertalk-04-gateway/supertalk-04-gateway-server',
                'target': 'ecc-04-gateway-server-{0}.war'.format(version),
                'release': 'gateway',
                'run': 'gateway_run.sh',
                'stop': 'gateway_stop.sh',
            },
            'legw': {
                'source': 'supertalk-04-legacy-gateway/supertalk-04-legacy-gateway-server',
                'target': 'ecc-04-legacy-gateway-server-{0}.war'.format(version),
                'release': 'legw',
                'run': 'legw_run.sh',
                'stop': 'legw_stop.sh',
            },
            'restapi': {
                'source': 'supertalk-20-rest-api-inmethod',
                'target': 'ecc-20-rest-api-inmethod-{0}.war'.format(version),
                'release': 'restapi',
                'run': 'restapi_run.sh',
                'stop': 'restapi_stop.sh',
            },
            'router': {
                'source': 'supertalk-04-router/supertalk-04-router-server',
                'target': 'ecc-04-router-server-{0}.war'.format(version),
                'release': 'router',
                'run': 'router_run.sh',
                'stop': 'router_stop.sh',
            },
            'rtis': {
                'source': 'supertalk-04-monitoring/supertalk-04-monitoring-rtis',
                'target': 'ecc-04-monitoring-rtis-{0}.war'.format(version),
                'release': 'rtis',
                'run': 'rtis_run.sh',
                'stop': 'rtis_stop.sh',
            },
            'scheduler': {
                'source': 'supertalk-10-scheduler',
                'target': 'ecc-10-scheduler-{0}.zip',
                'release': 'scheduler',
                'run': 'scheduler_run.sh',
                'stop': 'scheduler_stop.sh',
            },
            'webapps': {
                'source': 'supertalk-13-ui-webapps',
                'target': 'ecc-13-ui-webapps-{0}.war'.format(version),
                'release': 'webapps',
                'run': 'webapps_run.sh',
                'stop': 'webapps_stop.sh',
            },
            'thirdparty': {
                'source': 'supertalk-04-thirdparty/supertalk-04-thirdparty-server',
                'target': 'ecc-04-thirdparty-server-{0}.war'.format(version),
                'release': 'thirdparty',
                'run': 'thirdparty_run.sh',
                'stop': 'thirdparty_stop.sh',
            },
            'webroot': {
                'source': 'supertalk-21-ui-proxy',
                'target': 'ecc-21-ui-proxy-{0}.war'.format(version),
                'release': 'webroot',
                'run': 'webroot_run.sh',
                'stop': 'webroot_stop.sh',
            }
        }

    def translation(self, service_name, attribute):
        return self.pattern[service_name][attribute]

