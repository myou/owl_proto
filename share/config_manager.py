import os
import configparser

class ServerSvnConfig:

    def __init__(self):
        self.config = configparser.ConfigParser()
        config_file = os.path.join(os.path.dirname(__file__), 'conf/config.ini')
        self.config.read(config_file)

    def get(self, section, key):
        return self.config.get(section, key)

    def getKeyfile(self):
        dir = os.path.join(os.path.dirname(__file__), 'conf/key/')
        filenames = os.listdir(dir)
        full_filename = []

        for filename in filenames:
            full_filename.append(os.path.join(dir, filename))

        return full_filename

class SvnInfo:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

class ServerInfo:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)