from database_postgesql import Sqltemplate
from copy_data import Copydata
from query_builder import Querybuilder
from datetime import datetime

sqlmap = Sqltemplate(host='52.78.192.186',
                                port='5432',
                                database='KANTUKAN_TEST',
                                user='PA',
                                password='tmvprxmfk')

def main():
    startdate = '20171201000000'
    enddate = '20171231235959'

    createdateCondition = Querybuilder(sqltemplate=sqlmap)
    createdateCondition.append(target="created_date", condition=">=", value=startdate)
    createdateCondition.append(target="created_date", condition="<=", value=enddate)

    ticketIds = createdateCondition.getkeyids(key='ticket_id', target_table='t_ticket')
    talkqueryCondition = Querybuilder(sqltemplate=sqlmap)
    talkqueryCondition.appendsubquery(target="talk_id", condition="in", value=ticketIds)

    datarows = sqlmap.select(talkqueryCondition.getselectquery(target_table='t_talk_message'))

    for row in datarows:
        print(row)
        break

    sqlmap.close()


if __name__ == "__main__":
    main()