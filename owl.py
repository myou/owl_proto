from cli_module.command_reader import Commander
from adapter.router import Router

def main():
    module_name = Commander().req_param("select your module  > ")
    module = Router().load_module(module_name)
    module.Module().run()

if __name__ == "__main__":
    main()